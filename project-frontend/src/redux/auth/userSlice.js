import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    'jwtToken': "vivek"
}

export const userSlice = createSlice({
  name: 'userToken',
  initialState,
  reducers: {
    setToken: (state, action) => {
      state.jwtToken = action.payload
    },
    getToken: (state) => {
      return state.jwtToken
    },
  },
})

// Action creators are generated for each case reducer function
export const { setToken, getToken } = userSlice.actions

export default userSlice.reducer