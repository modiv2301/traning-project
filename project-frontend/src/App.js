import logo from './logo.svg';
import { useState } from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
} from "react-router-dom";
import Navbar from './components/Navbar';
import { Provider, useSelector, useDispatch } from 'react-redux'
import {store} from './store'
import {getToken, setToken} from './redux/auth/userSlice'
import List from './pages/List';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Blog from './pages/Blog';
import AddForm from './pages/AddForm';
import Test from './pages/Test';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Registration from './pages/Registration';
const UlCss = {
  'list-style':'none'
}
const App = () => {
  return (
    <div className="App">
      <Provider store={store}>
      <ToastContainer />
        <Router>  
          <Navbar />
          <Routes>
            <Route path="/" element={<List />} />
            <Route path="/list/:id" element={<Blog />} />
            <Route path="/add" element={<AddForm />} />
            <Route path="/test" element={<Test />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/registration" element={<Registration />} />
          </Routes>
        </Router>
      </Provider>
    </div>
  );
}


export default App;
