import { configureStore } from '@reduxjs/toolkit'
import userReducer from './redux/auth/userSlice'

export const store = configureStore({
  reducer: {
    user: userReducer,
  },
})