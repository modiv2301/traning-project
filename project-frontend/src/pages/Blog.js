import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { toast } from "react-toastify";
import axios from "axios"


const Blog = (props) => {
    const base_url = "http://localhost:3000"
    const [data, setData] = useState({});
    let {id} = useParams()
    useEffect(()=>{
        axios.get(base_url + '/api/blog/list/'+id)
            .then((response)=> {
                console.log(response)
                setData(response.data.message);
            })
            .catch((error)=> {
                toast.error("Something went wrong");
            })
    },[])
    return (
        <div >
            <div >
                <h3>{data.title}</h3>
                <p>{data.description}</p>
            </div>
        </div>
    )
}

export default Blog;