import axios from 'axios';
import { useEffect, useState } from 'react';
import '../index.css'
import { toast } from "react-toastify";
import {
    Link,
  } from "react-router-dom";
const List = () => {
    const base_url = "http://localhost:3000"
    const [data, setData] = useState([]);
    useEffect(()=>{
        axios.get(base_url + '/api/blog/list')
            .then((response)=> {
                setData(response.data.message);
            })
            .catch((error)=> {
                toast.error("Something went wrong");
            })
    },[])
    console.log(data)
    return (
        <>
            {data.map((blog, index) => {
                return (
                    <div key={index}>
                        <div >
                            <h3>{blog.title}</h3>
                            <p>{blog.description.substring(0, 200)}</p>
                        </div>
                        <Link to={'/list/'+blog._id} >See more...</Link>
                        <hr />
                    </div>
                )
            })}
        </>
    )
}
export default List;