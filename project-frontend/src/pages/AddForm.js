import axios from "axios"
import { useEffect, useState } from "react"
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const AddForm = () => {
    let navigate = useNavigate();
    const base_url = "http://localhost:3000"

    let [formData, setFormData] = useState({
        title: '',
        description: ''
    })
    // let [localStorageName, setLocalStorageName] = useState(localStorage.getItem('Name'))
    // useEffect(()=>{
    //     if(!localStorage.getItem('Name')) {
    //         navigate("/")
    //     }
    // },[])
    const submitForm = () => {
        axios.post(base_url + "/api/blog/add",formData, {
            headers: {
                authorization: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MmQ0YzE3YTc0MGUxOTJmY2VlNjFjNWYiLCJpYXQiOjE2NTgxMTAzNzAsImV4cCI6MTY1ODExMzk3MH0.GMQSOLjBbwH4zpAuv_Sg_VY__zFkJKe0HaAwzNuHV1A"
            }
        })
            .then(()=> {
                toast.success("Blog added successfully");
            })
            .catch((e)=> {
                toast.error("Something went wrong");
            })
    }
    return (
        <>
            <input type='text' style={{width:'50%', padding:'15px', fontSize:'20px'}} onChange={(e)=> setFormData({...formData, title: e.target.value})} value={formData.title}/>
            <br /><br />
            <textarea cols={100} rows={10} style={{width:'50%', padding:'15px', fontSize:'20px'}} onChange={(e)=> setFormData({...formData, description: e.target.value})}>{formData.description}</textarea><br /><br />
            <button onClick={submitForm}>Submit</button>
        </>
    )
}
export default AddForm