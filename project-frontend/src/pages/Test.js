import { useEffect, useState } from "react"

const Test = () => {
    const [data, setData] = useState('');
    const [data1, setData1] = useState('');
    const [data2, setData2] = useState('');
    useEffect(()=>{
        console.log("No dep")
    },[])

    useEffect(()=>{
        console.log("data")
    },[data])
    useEffect(()=>{
        console.log("data1")
    },[data1])
    useEffect(()=>{
        console.log("data2")
    },[data2])

    return (
        <>
            <h1>{data}</h1>
            <h1>{data1}</h1>
            <h1>{data2}</h1>
            <button onClick={()=> setData("vivek")}>Set data</button>
            <button onClick={()=> setData1("vivek")}>Set data</button>
            <button onClick={()=> setData2("vivek")}>Set data</button>
        </>
    )
}
export default Test