import axios from "axios"
import { useEffect, useState } from "react"
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const Registration = () => {
    let navigate = useNavigate();
    const base_url = "http://localhost:3000"

    let [formData, setFormData] = useState({
        name: '',
        mobile: '',
        email: '',
        password: ''
    })
    // let [localStorageName, setLocalStorageName] = useState(localStorage.getItem('Name'))
    // useEffect(()=>{
    //     if(!localStorage.getItem('Name')) {
    //         navigate("/")
    //     }
    // },[])
    const submitForm = () => {
        axios.post(base_url + "/api/auth/registration",formData)
            .then((response)=> {
                console.log(response.data.token)
                toast.success("Registered successfully");
                window.location.href = "/"
            })
            .catch((e)=> {
                toast.error("Something went wrong");
            })
    }
    return (
        <>
            Name: <input type='text' style={{width:'50%', padding:'15px', fontSize:'20px'}} onChange={(e)=> setFormData({...formData, name: e.target.value})} value={formData.name}/>
            <br /><br />
            Mobile: <input type='text' style={{width:'50%', padding:'15px', fontSize:'20px'}} onChange={(e)=> setFormData({...formData, mobile: e.target.value})} value={formData.mobile}/>
            <br /><br />
            Email: <input type='text' style={{width:'50%', padding:'15px', fontSize:'20px'}} onChange={(e)=> setFormData({...formData, email: e.target.value})} value={formData.email}/>
            <br /><br />
            Password: <input type='text' style={{width:'50%', padding:'15px', fontSize:'20px'}} onChange={(e)=> setFormData({...formData, password: e.target.value})} value={formData.password}/>
            <br /><br />
            <button onClick={submitForm}>Submit</button>
        </>
    )
}
export default Registration