import axios from "axios"
import { useEffect, useState } from "react"
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const Login = () => {
    let navigate = useNavigate();
    const base_url = "http://localhost:3000"

    let [formData, setFormData] = useState({
        email: '',
        password: ''
    })
    // let [localStorageName, setLocalStorageName] = useState(localStorage.getItem('Name'))
    // useEffect(()=>{
    //     if(!localStorage.getItem('Name')) {
    //         navigate("/")
    //     }
    // },[])
    const submitForm = () => {
        axios.post(base_url + "/api/auth/login",formData)
            .then((response)=> {
                console.log(response.data.token)
                toast.success("Login successfully");
                localStorage.setItem("jwt",response.data.token)
                window.location.href = "/"
            })
            .catch((e)=> {
                toast.error("Something went wrong");
            })
    }
    return (
        <>
            Email: <input type='text' style={{width:'50%', padding:'15px', fontSize:'20px'}} onChange={(e)=> setFormData({...formData, email: e.target.value})} value={formData.email}/>
            <br /><br />
            Password: <input type='text' style={{width:'50%', padding:'15px', fontSize:'20px'}} onChange={(e)=> setFormData({...formData, password: e.target.value})} value={formData.password}/>
            <br /><br />
            <button onClick={submitForm}>Submit</button>
        </>
    )
}
export default Login