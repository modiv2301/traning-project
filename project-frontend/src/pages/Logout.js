import { useEffect } from "react"

const Logout = () => {
    useEffect(()=> {
        localStorage.removeItem('jwt');
        window.location.href="/"
    },[ ])
} 
export default Logout;