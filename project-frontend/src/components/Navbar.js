
import { useState } from "react";
import {
    Link,
} from "react-router-dom";
const UlCss = {
'list-style':'none'
}

const Navbar = () => {
  const [jwt, setJwt] = useState(localStorage?.getItem('jwt'));
    return (
        <ul style={UlCss}>
          <li >
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/add">Add Blog</Link>
          </li>
          { !jwt ?
            <>
              <li>
                <Link to="/login">Login</Link>
              </li>
              <li>
                <Link to="/registration">Registration</Link>
              </li>
            </>
            : 
            <li>
              <Link to="/logout">Logout</Link>
            </li>}
        </ul>
    )
}
export default Navbar;