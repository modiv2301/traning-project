const mongoose =  require('mongoose');
const { Schema } = mongoose;
var ObjectId = require('mongodb').ObjectID;

const blogSchema = new Schema({
    title: {
        type: String,
        require: true,
        trim: true,
        maxlength: 50
    },
    description: {
        type: String,
        require: true,
        trim: true,
    },
    userId: {
        type: ObjectId,
        ref: "User"
    },
    likes: [{type: ObjectId, ref:"User"}]
},{timestamps: true});

// userSchema
//     .virtual("plain_password")
//     .set(function(password) {
//         this._password = password;
//         this.salt = uuidv1();
//     })

module.exports = mongoose.model("Blog", blogSchema)