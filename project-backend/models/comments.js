const mongoose =  require('mongoose');
const { Schema } = mongoose;

const commentsSchema = new Schema({
    userId: {
        type: ObjectId,
        ref: "User"
    },
    blogId: {
        type: ObjectId,
        ref: "Blog"
    },
    comment: {
        type: String,
        require: true,
        trim: true
    },
},{timestamps: true});

module.exports = mongoose.model("User", userSchema)