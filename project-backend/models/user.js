const mongoose =  require('mongoose');
const { Schema } = mongoose;
const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');

const userSchema = new Schema({
    name: {
        type: String,
        require: true,
        trim: true,
        maxlength: 50
    },
    photo: {
        type: String,
        require: false,
    },
    email: {
        type: String,
        require: true,
        unique: true,
    },
    encyrpt_password: {
        type: String,
        require: true,
        trim: true,
    },
    mobile: {
        type: Number,
        require: true,
        trim: true,
        maxlength: 12
    },
    salt: {
        type: String,
        require: true
    }
},{timestamps: true});
//modiv2301@gmail.com
userSchema
    .virtual("password")
    .set(function(password) {
        this._password = password;
        this.salt = uuidv4();
        this.encyrpt_password = this.securePassword(password)
    })
    .get(function() {
        return this._password;
    })

userSchema.methods = {
    authenticate: function(password) {
        return (this.securePassword(password) === this.encyrpt_password)
    },
    securePassword:  function(plainPassword) {
        if(!plainPassword) return "";
        try{
            return crypto
            .createHmac("sha256", this.salt)
            .update(plainPassword)
            .digest("hex")
        } catch(error) {
            return ""
        }
    }
}

module.exports = mongoose.model("User", userSchema)