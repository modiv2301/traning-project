const express = require('express')
const app = express()
const port = 3000;
const bodyParser = require("body-parser");
const cors = require("cors")
const mongoose = require('mongoose');
const authRoutes = require("./routes/authRoutes");
const blogRoutes = require('./routes/blogRoutes');
app.use(bodyParser.json());
app.use(cors());
mongoose.connect('mongodb://localhost:27017/blog').then(()=> {
  console.log("Mongodb connected")
});

app.use("/api/auth", authRoutes)
app.use("/api/blog", blogRoutes)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})