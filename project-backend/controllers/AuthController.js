const AuthController = {}
const User = require("../models/user")
const jwt = require("jsonwebtoken");
AuthController.registration = (req, res) => {
    // console.log(body);
    let user = new User(req.body);
    let result = {}
    user.save((err, insertedUser) => {
        if(err) {
            console.log(err);
            res.status(400).json({
                status: false, 
                message: "Something went wrong"
            })
        } else {
            res.json({
                status: true,
                message: "User registration successfully"
            })
        }
    })
    // console.log("here")
}

AuthController.login = (req, res) => {
    let user = new User();
    let password = user.securePassword(req.body.password);
    console.log(password,"password")
    User.find( {"email":req.body.email, "password": password}, (err, selectedUser) => {
        if(err) {
            console.log(err)
        } else {
            console.log(selectedUser[0]);
            selectedUser[0].email = undefined;
            selectedUser[0].mobile = undefined;
            selectedUser[0].__v = undefined;
            selectedUser[0].encyrpt_password = undefined;
            selectedUser[0].salt = undefined;
            if(selectedUser.length){
                let token = jwt.sign(
                    { userId: selectedUser[0]._id },
                    "secretkeyappearshere",
                    { expiresIn: "1h" }
                  );
                // console.log(token, "tokennnnnnnn")
                let finalUser = selectedUser[0];
                console.log(finalUser,"final userrrrr");
                // finalUser = {...finalUser, token};
                res.json({
                    status:true,
                    message: finalUser,
                    token: token
                })
            } else {
                res.status(400).send({
                    status: false,
                    message: "No user found"
                })
            }
        }
    } )
}

AuthController.profile = (req, res, userId) => {
    
    User.findById(userId,(err, user) => {
        if(err) {
            console.log(err);
            res.status(400).json({
                status: false, 
                message: "Something went wrong"
            })
        } else {
            user.__v = undefined;
            user.encyrpt_password = undefined;
            user.salt = undefined;
            res.json({
                status: true,
                message: user
            })
        }
    })
    // console.log("here")
}


module.exports = AuthController