const BlogController = {}
const Blog = require("../models/blog")
const jwt = require("jsonwebtoken");
BlogController.add = (req, res) => {
    // console.log(body);
    let blog = new Blog(req.body);
    let result = {}
    blog.save((err, insertedBlog) => {
        if(err) {
            console.log(err);
            res.status(400).json({
                status: false, 
                message: "Something went wrong"
            })
        } else {

            res.json({
                status: true,
                message: "Blog added successfully"
            })
        }
    })
    // console.log("here")
}

BlogController.list = (req, res) => {
    Blog.find((err, blogList) => {
        if(err) {
            console.log(err);
            res.status(400).json({
                status: false, 
                message: "Something went wrong"
            })
        } else {
            console.log(blogList)
            res.json({
                status: true,
                message: blogList
            })
        }
    })
    // console.log("here")
}

BlogController.blogById = (req, res) => {
    Blog.findById(req.params['id'],(err, blogList) => {
        if(err) {
            console.log(err);
            res.status(400).json({
                status: false, 
                message: "Something went wrong"
            })
        } else {
            
            res.json({
                status: true,
                message: blogList
            })
        }
    })
    // console.log("here")
}

BlogController.update = (req, res) => {
    Blog.findById(req.params['id'],(err, blogList) => {
        if(err) {
            console.log(err);
            res.status(400).json({
                status: false, 
                message: "Something went wrong"
            })
        } else {
            blogList.title = req.body.title;
            blogList.description = req.body.description;
            blogList.save((err, blog)=>{
                if(err) {
                    console.log(err);
                    res.status(400).json({
                        status: false, 
                        message: "Something went wrong"
                    })
                } else {
                    res.json({
                        status: true,
                        message: "Blog updated successfully"                
                    })
                }
            })
        }
    })
}


BlogController.delete = (req, res) => {
    Blog.findById(req.params['id'],(err, blogList) => {
        if(err) {
            console.log(err);
            res.status(400).json({
                status: false, 
                message: "Something went wrong"
            })
        } else {
           blogList.remove((err, blog) => {
                if(err) {
                    console.log(err);
                    res.status(400).json({
                        status: false, 
                        message: "Something went wrong"
                    })
                } else {
                    res.json({
                        status: true,
                        message: "Blog deleted successfully"                
                    })
                }
           })
        }
    })
}

module.exports = BlogController