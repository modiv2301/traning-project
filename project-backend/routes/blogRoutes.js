const express = require("express");
const blogRoutes = express.Router();
const BlogController = require("../controllers/BlogController")
const jwt = require("jsonwebtoken");


blogRoutes.post("/add", async (req, res) => {
    const decoded = jwt.verify(req.headers.authorization, "secretkeyappearshere");  
    req = {...req, userId: decoded.userId}
    const result = await BlogController.add(req, res);
})


blogRoutes.get("/list", async (req, res) => {
    const result = await BlogController.list(req, res);
})


blogRoutes.get("/list/:id", async (req, res) => {
    const result = await BlogController.blogById(req, res);
})


blogRoutes.put("/add/:id", async (req, res) => {
    const decoded = jwt.verify(req.headers.authorization, "secretkeyappearshere");  
    req = {...req, userId: decoded.userId}
    const result = await BlogController.update(req, res);
})


blogRoutes.delete("/delete/:id", async (req, res) => {
    const decoded = jwt.verify(req.headers.authorization, "secretkeyappearshere");  
    req = {...req, userId: decoded.userId}
    const result = await BlogController.delete(req, res);
})

module.exports = blogRoutes;