const express = require("express");
const categoryRoutes = express.Router();
const AuthController = require("../controllers/AuthController")
const jwt = require("jsonwebtoken");


categoryRoutes.post("/registration", async (req, res) => {
    const result = await AuthController.registration(req, res);
})

categoryRoutes.post("/login", async (req, res) => {
    const result = await AuthController.login(req, res);
})

categoryRoutes.get("/profile", async (req, res) => {
    const decoded = jwt.verify(req.headers.authorization, "secretkeyappearshere");  
    let userId = decoded.userId;
    const result = await AuthController.profile(req, res, userId);
})

module.exports = categoryRoutes;